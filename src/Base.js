import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Loadable from 'react-loadable';
import './Base.css';

const Loading = () => <div>Loading...</div>;

var sort = null;

class Base extends Component {
  render() {
    return (
      <div className="Base">
        <div className="baseCard">
        <p className="header-text">
          <span>GrabSave</span></p><br></br>
          <span>Track your savings and spendings with Grab!</span>
          <img src="dolla@2x.jpg" class="money" alt="money" />
          <span>How much did you spend today?</span><br></br>
          <span> All in one expense tracker at the heart of all your activities?</span>
          <a href="/spend"> See More </a>
        </div>
      </div>
    );
  }
}
export default Base;