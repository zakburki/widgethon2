import React, { Component } from 'react';
import { BarChart } from 'react-d3-components';
import { PieChart } from 'react-d3-components'
import { LineChart } from 'react-d3-components'
import { Brush } from 'react-d3-components'
import { d3 } from 'react-d3-components';
import './Widget.css';

var colorScale = d3.scale.category20c().domain([0, 100]);
var colorScale2 = d3.scale.category10().domain([100, 0]);


var colorScale3 = d3.scale.ordinal().range(["#FF6771", "#28D1CF", "#84D29A", "#5B96CF", "#FFCE59"]);
var colorScale4 = d3.scale.ordinal().range(["#F8C0B9", "#F4A197", "#F18274"]);

const API = 'http://frozen-ravine-39615.herokuapp.com/1/monthwise/d3';
const API2 = 'http://localhost:5000/static/spend.json';
const API3 = 'http://frozen-ravine-39615.herokuapp.com/1/cumulative?month=6';

var piedata = {
        label: 'Spend Breakdown',
        values: [{x: 'GrabRides', y: 10}, {x: 'GrabFood', y: 4}, {x: 'GrabPay', y: 3}]
};
var sort = null;
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
    spend: [{
    "label": "GrabRides",
    "values": [{"x": "March", "y": 0}, {"x": "April", "y": 0}, {"x": "May", "y": 0},{"x": "June", "y": 0},{"x": "July", "y": 0}],
      "style": {
        "color": "green",
        "opacity": 1.0
      }
    },
    {
    "label":"GrabFood",
    "values": [{"x": "March", "y": 0}, {"x": "April", "y": 0}, {"x": "May", "y": 0},{"x": "June", "y": 0},{"x": "July", "y": 0}],
      "style": {
        "color": "blue",
        "opacity": 1.0
      }
    },
    {
    "label": "GrabPay",
    "values": [{"x": "March", "y": 0}, {"x": "April", "y": 0}, {"x": "May", "y": 0},{"x": "June", "y": 0},{"x": "July", "y": 0}],
      "style": {
        "color": "yellow",
        "opacity": 1.0
      }
    },
    {
    "label": "Predicted",
    "values": [{"x": "March", "y": 0}, {"x": "April", "y": 0}, {"x": "May", "y": 0},{"x": "June", "y": 0},{"x": "July", "y": 0}],
      "style": {
        "color": "red",
        "opacity": 0.2
      }
    }],
    limit:[
    {
    label: 'Current Spend',
    values: [{x: 'July', y: 100}],
    style: {
      color: 'green',
      opacity: 1.0
    }
    },
    {
    label: 'Remaining',
    values: [{x: 'July', y: 600}],
    style: {
      color: 'green',
      opacity: 1.0
    }
    }
    ],
    line: { values: [
                {x:1, y: 37},
                {x:2, y: 80},
                {x:3, y: 112},
                {x:4, y: 140},
                {x:5, y: 166},
                {x:6, y: 191},
                {x:7, y: 220},
                {x:8, y: 247},
                {x:9, y: 260},
                {x:10, y: 280},
                {x:11, y: 291},
                {x:12, y: 346},
                {x:13, y: 365},
                {x:14, y: 380},
                {x:15, y: 430},
                {x:16, y: 470},
                {x:17, y: 503},
                {x:18, y: 540},
                {x:19, y: 571},
                {x:20, y: 603},
                {x:21, y: 658},
                {x:22, y: 701},
                {x:23, y: 748},
                {x:24, y: 789},
                {x:25, y: 800},
                {x:26, y: 850},
                {x:27, y: 867},
                {x:28, y: 899},
                {x:29, y: 956},
                {x:30, y: 981}
      ]},
      xScale: d3.time.scale().domain([1, 30]).range([0, 300]),
      isLoading: false,
      error: null,
      budget: 500,
      remainder: 300,
      lotto:true,
      lottotry:false,
    };

    this.playLotto = this.playLotto.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleChange(event) {
    this.setState({budget: event.target.value});
  }
  handleSubmit(event) {
    event.preventDefault();
    this.setState({budget: event.target.value});
  }
  playLotto(event){
      if(this.state.lottotry){
        this.setState({lotto: true})
      }else{
      if(Math.floor(Math.random()*8) < 3){
        this.setState({lottotry: false})
        this.setState({lotto: false });
      }else{
        this.setState({lottotry: false})
        this.setState({lotto: true});
      }
    }
  }
  componentDidMount() {
    this.setState({ isLoading: true });
    
    fetch(API)
      .then(response => response.json())
      .then(data => this.setState({ spend: data, isLoading: false }))
      .catch(error => this.setState({ error, isLoading: false }));

    fetch(API3)
      .then(response => response.json())
      .then(data => this.setState({ line: data, isLoading: false }))
      .catch(error => this.setState({ error, isLoading: false }));
  }
  render() {
    const { spend , isLoading } = this.state;
    if (isLoading) {
      return <p>Loading ...</p>;
    }
    return (
      <div className="App">
      <div className="MainText" >PROMO CODE LOTTERY</div>
      <div className="center"> 
        <span className="Card" style={{width:'200px',height:'150px'}}>
          <img onClick={this.playLotto} style={{width:'75px'}} data-index="1" src="grab-logo-png-transparent.jpg"/>
        </span>
        <span className="Card" style={{width:'200px',height:'150px'}}>
          <img onClick={this.playLotto} style={{width:'75px'}} data-index="1" src="grab-logo-png-transparent.jpg"/>
        </span>
        <span className="Card" style={{width:'200px',height:'150px'}}>
          <img onClick={this.playLotto} style={{width:'75px'}} data-index="1" src="grab-logo-png-transparent.jpg"/>
        </span>
      </div>
      <div className="MainText"> { !this.state.lottotry && this.state.lotto ? <span>CHOOSE YOUR CARD!</span>:<span></span>}</div>
      <div className="tryTest"> { this.state.lottotry && this.state.lotto ? <span> Sorry, Try Your Luck again tomorrow!</span>: <span></span> } </div>
      <div className="winText"> { this.state.lotto ? <span></span> : <span>You Win! PROMO CODE GRABWIDGETHON10</span> } </div>
      <div>

      </div>
        <div style={{marginTop: 2 + 'em'}}></div>
        <form>
          <label>
            <span style={{marginRight:'10px'}}>Set your monthly Budget?</span>
            <input type="number" value={this.state.value} onChange={this.handleChange} />
          </label>
        </form>
        <div>Your monthly budget is currently set at a default {this.state.budget}</div>
        
        <BarChart
        data={this.state.limit}
        colorScale={colorScale3}
        width={300}
        height={300}
        margin={{top: 50, bottom: 50, left: 50, right: 10}}
        horizontal={true}
        yAxis={{label: "Spend in SGD"}}
        />
 
        <div> Your monthly spend trend</div>

        <BarChart
        data={this.state.spend}
        colorScale={colorScale3}
        width={300}
        height={300}
        margin={{top: 50, bottom: 50, left: 50, right: 10}}
        horizontalline={300}
        groupedBars={false}
        xLabel={"Month"}
        yAxis={{label: "Spend in SGD"}}
        LABELS ={["Public", "Private"]}
        />
      
      <PieChart
        data={piedata}
        colorScale={colorScale4}
        width={400}
        height={400}
        margin={{top: 10, bottom: 10, left: 100, right: 100}}
        sort={sort}
        chartLabel={{label:"June"}}
        />

      <LineChart
         data={this.state.line}
         width={400}
         height={400}
         margin={{top: 10, bottom: 50, left: 50, right: 20}}
         xAxis={{innerTickSize: 1, label: "# Day in June"}}
         yAxis={{label: "SGD"}}
      />
      </div>
    );
  }
}
export default App;