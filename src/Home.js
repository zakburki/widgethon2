import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Loadable from 'react-loadable';
import Widget from './Widget';
import Base from './Base';

const Loading = () => <div>Loading...</div>;

const Card = Loadable({
  loader: () => import('./Base'),
  loading: Loading,
});

const Spend = Loadable({
  loader: () => import('./Widget'),
  loading: Loading,
});

const Home = () => (
  <Router>
    <Switch>
      <Route exact path="/" component={Base}/>
      <Route path="/spend" component={Widget}/>
    </Switch>
  </Router>
);

export default Home;