import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Widget from './Widget';
import Base from './Base';
import Home from './Home';

import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<Home />, document.getElementById('root'));
registerServiceWorker();
