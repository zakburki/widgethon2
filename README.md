##PRE-REQUISITES

You need Node.js and Yarn package manager installed on your machine first.

https://nodejs.org/en/ and https://yarnpkg.com/lang/en/


##Steps to run widget on your laptop.

### Open terminal in your Macbook

### clone the repo. 
`> git clone https://bitbucket.org/teamwidgethon/widgethon2.git`

### navigate to the repo 
`> cd widgethon2`

### install dependencies 
`> yarn`

### run the widget
`> yarn start`

### if the app doesn't automatically start in the browser navigate to http://localhost:3000 in Chrome/Firefox.

